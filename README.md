dev test project
===============

Projet **Symfony 3.4** à télécharger. Utiliser **composer** pour télécharger les dependances (https://symfony.com/doc/3.4/setup.html#updating-symfony-applications), puis lancer le serveur interne (https://symfony.com/doc/3.4/setup.html#running-the-symfony-application).
Pas besoin d'avoir une connaissance approfondie de Symfony, une lecture du code devrait suffire pour debuger ou ajouter les fonctionnalités demandées. 

Lors de l'installation composer, accepter les parametres de DB proposés par defaut.
Il n'y a aucune installation/configuration/modification à faire directement sur la base de données ou via un outil de type phpMyAdmin. L'exercice porte uniquement sur PHP.

Pour toute question contacter <isd@bak2group.com>

Cette micro app permet de créer/mettre-à-jour/lister des fabricants (manufacturer), des modèles (model) et des produits (product).

Une fois le projet installé, les differentes entités sont accessibles par les urls (routes) suivantes (ip/host et port à changer suivant votre configuration)

- http://127.0.0.1:8000/model
- http://127.0.0.1:8000/model/new
- http://127.0.0.1:8000/model/{id}/show
- http://127.0.0.1:8000/model/{id}/edit

- http://127.0.0.1:8000/manufacturer
- http://127.0.0.1:8000/manufacturer/new
- http://127.0.0.1:8000/manufacturer/{id}/show
- http://127.0.0.1:8000/manufacturer/{id}/edit

- http://127.0.0.1:8000/product
- http://127.0.0.1:8000/product/new
- http://127.0.0.1:8000/product/{id}/show
- http://127.0.0.1:8000/product/{id}/edit



##### Bugs

Le projet contient **quelques bugs** à fixer (au minimum 4), certains bloquants dés l'ouverture du projet, d'autres suivant les fonctions appelées).


##### Routing
- Remplacer les chemins de type **/manufacturer/{id}/show** par **/manufacturer/{id}** (idem pour model et product)
- Accès aux formulaire d'encodage (new) et update (edit) derrière **/admin** donc modifier les routes pour avoir **/admin/manufacturer/new**, **/admin/manufacturer/{id}/edit**. Idem pour **model** et **product**


##### Admin/Encodage/Formulaires
- Encodage product: donner le grade en fonction d'une liste: A,B ou C
- Encodage product: donner la couleur en fonction de l'entité couleur existante, dropdown select par ordre alphabétique de couleurs, avec majuscule en début de nom (donc de type Black, Blue, Grey, Red, White).
Les couleurs sont déja dans la base de données.


##### Api/Json
- Retourner les listes (de type **/manufacturer**), detail (ex **/manufacturer/{id}**) en json si chemin de type **/api/**... Donc ajouter des routes de type **/api/product**, **/api/product/{id}**. Idem pour **manufacturer** et **model**.
- Pouvoir filtrer, dans le cas des products, par le modelId par l'url de type **/api/product?model={model_id}**
- Dans les json **/api/product** et **/api/product/{id}** ajouter un champ **uname** de type **MANUFACTURER_MODEL_GRADE_COLOR** (en uppercase, concatenation du manufacturer, model, des champs grade et color). Ce champ n'apparait pas dans la DB, juste en lecture.
- Le champs 'capacity' de l'entité product est facultatif. Retourner '**n/a**' partout où ce champ est retourné (json) et non renseigné ou null ou égal à 0.
